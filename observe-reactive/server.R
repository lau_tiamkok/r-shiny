library(shiny)

# Define server logic required to draw a histogram
shinyServer(function(input, output, session) {

  # output$plot1Ui <- renderUI({
  #   radioButtons(
  #       inputId = "plot1",
  #       label = "Plot 1:",
  #       choices = c(
  #           "Option 1" = "1",
  #           "Option 2" = "2",
  #           "Option 3" = "3"
  #       ),
  #       selected = NULL,
  #       inline = FALSE
  #   )
  # })

  # output$plot2Ui <- renderUI({
  #   radioButtons(
  #       inputId = "plot2",
  #       label = "Plot 2:",
  #       choices = c(
  #           "Option A" = "A",
  #           "Option B" = "B",
  #           "Option C" = "C"
  #       ),
  #       selected = character(0),
  #       inline = FALSE
  #   )
  # })

  observe({
      plot1 <- input$plot1
      if (!is.null(plot1) && plot1 != '1') {
          updateRadioButtons(
            session,
            "plot2",
            label = "Plot 2:",
            choices = c(
                # "None selected" = "",
                "Option A" = "A",
                "Option B" = "B",
                "Option C" = "C"
            ),
            selected = character(0),
            inline = FALSE
        )
      }
  })

  observe({
      plot2 <- input$plot2
      if (!is.null(plot2)) {
          updateRadioButtons(
            session,
            "plot1",
            label = "Plot 1:",
            choices = c(
                "Option 1" = "1",
                "Option 2" = "2",
                "Option 3" = "3"
            ),
            selected = character(0),
            inline = FALSE
        )
      }
  })

  # Call this only when the button is pressed.
  eventPlot <- eventReactive(input$goPlot, {
    cat('\n')
    cat('Plot 1:')
    str(input$plot1)

    cat('\n')
    cat('Plot 2:')
    str(input$plot2)
  })

  output$plot <- renderPlot({
      # render the plot from eventReactive.
      eventPlot()
  })
})
