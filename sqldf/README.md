# sqldf

1. Install sqldf & tcltk

    `install.packages("sqldf")`

    `install.packages("tcltk")`

2. Then in each R session where you want to use them,

    `library("sqldf")`
    `library("tcltk")`

## ref:

    * http://www.burns-stat.com/translating-r-sql-basics/
    * http://renkun.me/blog/2014/02/07/use-sql-to-operate-r-data-frames.html
    * http://shiny.rstudio.com/reference/shiny/latest/renderPrint.html
    * http://www.statmethods.net/input/contents.html
    * http://stackoverflow.com/questions/2018480/what-is-the-equivalent-of-var-dump-in-r
